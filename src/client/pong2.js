class Pong {
    constructor(client) {
        this.game = new Phaser.Game(500, 500, Phaser.AUTO, 'pong2', this);
        this.client = client;
    }

    preload() {
        this.game.stage.backgroundColor = '#222222';

        this.game.load.image('dust', 'assets/dust.png');
        this.game.load.image('exp', 'assets/exp.png');
        this.game.load.image('paddle', 'assets/paddle.png');
        this.game.load.image('ball', 'assets/ball.png');

        this.game.load.audio('collide', ['assets/dust.wav', 'assets/dust.mp3']);
        this.game.load.audio('goal', ['assets/dead.wav', 'assets/dead.mp3']);
    }

    create() {
        this.game.time.advancedTiming = true;
        this.game.physics.startSystem(Phaser.Physics.ARCADE);
        this.game.physics.arcade.checkCollision.up = false;
        this.game.physics.arcade.checkCollision.down = false;

        this.stage.disableVisibilityChange = true;

        this.spaceKey = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        this.cursor = this.game.input.keyboard.createCursorKeys();
        this.game.input.keyboard.addKeyCapture([Phaser.Keyboard.RIGHT, Phaser.Keyboard.LEFT, Phaser.Keyboard.SPACEBAR]);

        this.collideSound = this.game.add.audio('collide', 0.1);
        this.goalSound = this.game.add.audio('goal', 0.1);

        this.playerOne = this.game.add.sprite(this.game.world.centerX, this.game.height, 'paddle');
        this.playerOne.anchor.setTo(0.5, 1);
        this.game.physics.arcade.enable(this.playerOne);
        this.playerOne.body.collideWorldBounds = true;
        this.playerOne.body.bounce.set(1);
        this.playerOne.body.immovable = true;

        this.ball = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'ball');
        this.ball.anchor.setTo(0.5, 0.5);
        this.ball.checkWorldBounds = true;
        this.game.physics.arcade.enable(this.ball);
        this.ball.body.collideWorldBounds = true;
        this.ball.body.bounce.set(1);
        this.ball.events.onOutOfBounds.add(this.ballLost, this);

        this.playerTwo = this.game.add.sprite(this.game.world.centerX, 0, 'paddle');
        this.playerTwo.anchor.setTo(0.5, 0);
        this.game.physics.arcade.enable(this.playerTwo);
        this.playerOne.body.collideWorldBounds = true;
        this.playerTwo.body.bounce.set(1);
        this.playerTwo.body.immovable = true;

        this.dust = this.game.add.emitter(0, 0, 20);
        this.dust.makeParticles('dust');
        this.dust.setYSpeed(-100, 100);
        this.dust.setXSpeed(-100, 100);
        this.dust.gravity = 0;
        this.exp = this.game.add.emitter(0, 0, 20);
        this.exp.makeParticles('exp');
        this.exp.setYSpeed(-150, 150);
        this.exp.setXSpeed(-150, 150);
        this.exp.gravity = 0;

        this.introText = this.game.add.text(this.game.world.centerX, 400, '- hit space to start -', {
            font: "20px Arial",
            fill: "#ffffff",
            align: "center"
        });
        this.introText.anchor.setTo(0.5, 0.5);

        this.stopped = true;
        this.gameEnded = false;

        if (this.client.isPlaying()) {
            this.spaceKey.onDown.add(this.releaseBall, this);
        }
    }

    update() {
        this.game.physics.arcade.collide(this.ball, this.playerOne, this.ballHitPaddle, null, this);
        this.game.physics.arcade.collide(this.ball, this.playerTwo, this.ballHitPaddle, null, this);

        if (this.client.isPlaying()) {
            /*
            this.playerOne.x = this.game.input.x;
            if (this.playerOne.x > 500 - 60 / 2) this.playerOne.x = 500 - 60 / 2;
            if (this.playerOne.x < 60 / 2) this.playerOne.x = 60 / 2;
              */

            let playerPaddle;
            if (this.client.isPlayerOne()) {
                playerPaddle = this.playerOne;
            } else if (this.client.isPlayerTwo()) {
                playerPaddle = this.playerTwo;
            }

            let moved = this.oldX !== playerPaddle.x;
            if (this.cursor.left.isDown) {
                playerPaddle.body.velocity.x = -200;
            }
            else if (this.cursor.right.isDown) {
                playerPaddle.body.velocity.x = 200;
            }
            else {
                playerPaddle.body.velocity.x = 0;
            }
            if (moved) {
                this.client.sendPosition({
                    x: playerPaddle.x
                });
            }
            this.oldX = playerPaddle.x
        }

        // AI follow ball x
        // this.playerTwo.body.velocity.x = 400 * this.game.math.clamp((this.ball.x - this.playerTwo.x) / 20, -1, 1);

        this.exp.forEachAlive(function (p) {
            p.alpha = this.game.math.clamp(p.lifespan / 100, 0, 1);
        }, this);
    }

    movePaddlePlayerOne(x) {
        this.playerOne.x = x;
    }
    
    movePaddlePlayerTwo(x) {
        this.playerTwo.x = x;
    }

    setBallVelocity(clientId, data) {
        // // console.log("Velocity update", data);
        // if (this.client.isPlayerTwo() && this.client.playerTwo !== clientId) {
        //     this.ball.body.velocity.x = -data.velocity.x;
        //     this.ball.body.velocity.y = -data.velocity.y;
        // } else {
            this.ball.body.velocity.x = data.velocity.x;
            this.ball.body.velocity.y = data.velocity.y;
        // }
    }

    render() {
        this.game.debug.text(this.game.time.fps, 0, 40, "#0f0");
    }

    ballHitPaddle(ball, paddle) {
        let diff = 0;
        if (ball.x < paddle.x) {
            //  Ball is on the left-hand side of the paddle
            diff = paddle.x - ball.x;
            ball.body.velocity.x = (-20 * diff);
        }
        else if (ball.x > paddle.x) {
            //  Ball is on the right-hand side of the paddle
            diff = ball.x - paddle.x;
            ball.body.velocity.x = (20 * diff);
        }
        else {
            //  Ball is perfectly in the middle
            //  Add a little random X to stop it bouncing straight up!
            ball.body.velocity.x = 2 + Math.random() * 20;
        }
        this.collideSound.play();
        this.dust.x = this.ball.x;
        this.dust.y = this.ball.y;
        this.dust.start(true, 300, null, 8);
    }

    ballLost(sprite) {
        const playerScored = sprite.y < 0;

        // send ball lost
        if (this.client.isPlayerOne()) {
            this.client.sendGoal(playerScored);
        }

        // visuals
        let direction = playerScored ? 10 : -10;
        this.game.camera.shake(0.02, 70, true, Phaser.Camera.SHAKE_BOTH, false);
        this.exp.x = sprite.x;
        this.exp.y = sprite.y + direction;
        this.exp.start(true, 300, null, 20);

        // this.scoreText.text = this.score;
        // this.stopped = true;
        // this.goalSound.play();

        // const MAX_GOALS = 1;
        // if (this.score.playerOne === MAX_GOALS) {
        //     this.gameOver("You Win!");
        // } else if (this.score.playerTwo === MAX_GOALS) {
        //     this.gameOver("You lose!");
        // } else {
        //     this.ball.reset(this.game.world.centerX, this.game.world.centerY);
        // }
    }

    updateScore(scorer) {
        this.score.increment(scorer);
        this.scoreText.text = this.score;
        this.ball.reset(this.game.world.centerX, this.game.world.centerY);
        setTimeout(() => {
            const x = this.game.rnd.sign() * 75;
            const y = this.game.rnd.sign() * 300;
            this.client.sendBallVelocity({
                velocity: {x, y},
            });
        }, 3000);
    }

    releaseBall() {
        // if (this.gameEnded) {
        //     this.score = new Score();
        //     this.scoreText.text = this.score;
        //     this.ball.reset(this.game.world.centerX, this.game.world.centerY);
        //     this.gameEnded = false;
        // }

        if (this.stopped && this.client.isPlayerOne()) {
            this.client.sendGameStarted();
            this.sendRandomBallDirection();
        }
    }

    sendRandomBallDirection() {
        const x = this.game.rnd.sign() * 75;
        const y = this.game.rnd.sign() * 300;
        this.client.sendBallVelocity({
            velocity: {x, y},
        });
    }

    gameStarted() {
        this.stopped = false;
        this.introText.visible = false;

        this.score = new Score(this.client.playerOne, this.client.playerTwo);
        this.scoreText = this.game.add.text(32, 350, this.score, {font: "20px Arial", fill: "#ffffff", align: "left"});
        this.scoreText.visible = true;
    }

    gameOver(text) {
        this.ball.body.velocity.setTo(0, 0);
        this.introText.text = text;
        this.introText.visible = true;
        this.gameEnded = true;
    }

    shakeEffect(g, time) {
        var move = 5;

        this.game.add.tween(g)
            .to({y: "-" + move}, time).to({y: "+" + move * 2}, time * 2).to({y: "-" + move}, time)
            .to({y: "-" + move}, time).to({y: "+" + move * 2}, time * 2).to({y: "-" + move}, time)
            .to({y: "-" + move / 2}, time).to({y: "+" + move}, time * 2).to({y: "-" + move / 2}, time)
            .start();

        this.game.add.tween(g)
            .to({x: "-" + move}, time).to({x: "+" + move * 2}, time * 2).to({x: "-" + move}, time)
            .to({x: "-" + move}, time).to({x: "+" + move * 2}, time * 2).to({x: "-" + move}, time)
            .to({x: "-" + move / 2}, time).to({x: "+" + move}, time * 2).to({x: "-" + move / 2}, time)
            .start();
    }
}