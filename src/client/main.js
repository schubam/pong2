const HOST = location.origin.replace(/^http/, 'ws');
const connectionManager = new ConnectionManager();
connectionManager.connect(HOST);

const pong = new Pong(connectionManager);
connectionManager.game = pong;


function connectedPlayers(data) {
    const p = document.getElementById("player-id");
    const clients = data.peers.clients.map(client => client.id);
    s = '<ul>';
    s += `<li>You: ${data.peers.you}</li>`;
    s += `<li>Player One: ${data.peers.playerOne}</li>`;
    s += `<li>Player Two: ${data.peers.playerTwo}</li>`;
    s += '</ul>';
    s += `Connected clients (${clients.length}):`;
    s += '<ul>';
    clients.forEach(c => s += `<li>${c}</li>`);
    s += '</ul>'
    p.innerHTML = s;
}
