class ConnectionManager {
    constructor() {
        this.conn = null;
        this.peers = new Map;
        this.playerId = null;
        this.playerOne = null;
        this.playerTwo = null;
        this.game = null;
    }

    connect(address) {
        this.conn = new WebSocket(address);
        this.conn.addEventListener('open', () => {
            //console.log('Connection established');
            this.initSession();
        });

        this.conn.addEventListener('message', event => {
            // console.log('Received message', event.data);
            this.receive(event.data);
        });
    }

    initSession() {
        const sessionId = this.sessionId();
        if (sessionId) {
            this.send({
                type: 'join-session',
                id: sessionId
            });
        } else {
            this.send({
                type: 'create-session',
            });
        }
    }

    sessionId() {
        return window.location.hash.split('#')[1];
    }

    sendPosition(msg) {
        const data = msg;
        this.send({
            type: 'player-position',
            id: this.sessionId(),
            clientId: this.playerId,
            x: data.x,
        });
    }

    sendBallVelocity(msg) {
        const data = msg;
        this.send({
            type: 'ball-velocity',
            id: this.sessionId(),
            clientId: this.playerId,
            velocity: data.velocity,
        });
    }

    sendGameStarted() {
        this.send({
            type: 'game-started',
            id: this.sessionId(),
        });
    }

    sendGoal(playerScored) {
        const scorer = playerScored ? this.playerOne : this.playerTwo;
        // assert(scorer === this.playerOne);
        this.send({
            type: 'goal-scored',
            id: this.sessionId(),
            clientId: scorer
        });
    }

    receive(msg) {
        const data = JSON.parse(msg);
        if (data.type === 'session-created') {
            window.location.hash = data.id;
            this.playerId = data.you;
            this.playerOne = data.playerOne;
            this.playerTwo = data.playerTwo;
            //console.log("Session created, set player id: " + this.playerId);
            //console.log("Player One: " + this.playerOne);
            //console.log("Player Two: " + this.playerTwo);
        } else if (data.type === 'ball-velocity') {
            //console.log("Setting ball velocity", data);
            this.game.setBallVelocity(data.clientId, data);
        } else if (data.type === 'game-started') {
            this.game.gameStarted();
        } else if (data.type === 'goal-scored') {
            const scorer = data.clientId;
            //console.log(`Goal scored by ${scorer}`);
            this.game.updateScore(scorer);
        } else if (data.type === 'client-position') {
            // console.log("Setting client position", data);
            const clientId = data.clientId;
            const x = data.x;

            if (this.playerOne === clientId) {
                if (!this.isPlayerOne()) {
                    this.game.movePaddlePlayerOne(x);
                }
            } else if (this.playerTwo === clientId) {
                if (!this.isPlayerTwo()) {
                    this.game.movePaddlePlayerTwo(x);
                }
            } else {
                throw new Error('Unknown client position');
            }
        } else if (data.type === 'session-broadcast') {
            this.playerId = data.peers.you;
            this.playerOne = data.peers.playerOne;
            this.playerTwo = data.peers.playerTwo;
            //console.log("Session broadcast, set player id: " + this.playerId);
            //console.log("Player One: " + this.playerOne);
            //console.log("Player Two: " + this.playerTwo);
            connectedPlayers(data);
        }
    }

    send(data) {
        const msg = JSON.stringify(data);
        //console.log("Sending message", msg);
        this.conn.send(msg);
    }

    isPlaying() {
        return this.isPlayerOne() || this.isPlayerTwo();
    }

    isPlayerTwo() {
        return this.playerId === this.playerTwo;
    }

    isPlayerOne() {
        return this.playerId === this.playerOne;
    }
}