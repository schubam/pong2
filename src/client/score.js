class Score {
    constructor(p1, p2) {
        this.playerOneId = p1;
        this.playerTwoId = p2;
        this.scores = new Map();
        this.scores[p1] = 0;
        this.scores[p2] = 0;
    }

    increment(playerId) {
        this.scores[playerId] = this.scores[playerId] + 1;
    }

    toString() {
        return `${this.scores[this.playerOneId]}:${this.scores[this.playerTwoId]}`;
    }
}