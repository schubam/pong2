class Session {
    constructor(id) {
        this.id = id;
        this.clients = new Set;
        this.playerOne = null;
        this.playerTwo = null;
    }

    join(client) {
        if (client.sessions) {
            throw new Error('Client already in sessions');
        }
        this.clients.add(client);
        client.session = this;

        if (!this.playerOne) {
            this.playerOne = client.id;
        } else if (!this.playerTwo) {
            this.playerTwo = client.id;
        } else {
            //console.log('Adding spectator', client.id);
        }
    }

    leave(client) {
        if (client.session != this) {
            throw new Error('Client not in session');
        }

        if (this.playerOne === client.id) {
            this.playerOne = null;
        } else if (this.playerTwo === client.id) {
            this.playerTwo = null;
        }  else {
            //console.log('Spectator left', client.id);
        }
        
        this.clients.delete(client);
        client.session = null;
    }
}

module.exports = Session;