class Client {
    constructor(conn, id) {
        this.conn = conn;
        this.id = id;
        this.session = null;

        this.state = {
            arena: {},
            ball: {},
            player: {},
        };
    }

    send(data) {
        const msg = JSON.stringify(data);
        //console.log(`Sending message ${msg}`);
        this.conn.send(msg, function ack(err) {
           if (err) {
               //console.log('Error sending message', msg, err);
           }
        });
    }
}

module.exports = Client;