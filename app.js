const express = require('express');
const http = require('http');
const WebSocket = require('ws');

const Session = require('./src/server/session');
const Client = require('./src/server/client');

const app = express();
app.set('port', (process.env.PORT || 3000));
app.use('/css',express.static(__dirname + '/css'));
app.use('/js',express.static(__dirname + '/js'));
app.use('/src',express.static(__dirname + '/src'));
app.use('/assets',express.static(__dirname + '/assets'));

const server = http.createServer(app);
const wss = new WebSocket.Server({ server });

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/index.html');
});

const port = app.get('port');
server.listen(port, function() {
    console.log(`listening on *:${port}`);
});

const sessions = new Map;

function createId(length = 6, chars = 'abcdefghijklmnopqrstuvwxyz0123456789') {
    let id = '';
    while (length--) {
        id += chars[Math.random() * chars.length | 0];
    }
    return id;
}

function createClient(conn, id = createId()) {
    return new Client(conn, id);
}

function createSession(id = createId()) {
    if (sessions.has(id)) {
        throw new Error(`Session ${id} already exists`);
    }

    const session = new Session(id);
    console.log("Creating session", session);

    sessions.set(id, session);

    return session;
}

function getSession(id) {
    return sessions.get(id);
}

function broadcastSession(session) {
    const clients = [...session.clients];
    clients.forEach(client => {
        client.send({
            type: 'session-broadcast',
            peers: {
                you: client.id,
                playerOne: session.playerOne,
                playerTwo: session.playerTwo,
                clients: clients.map(client => {
                    return {
                        id: client.id,
                    }
                }),
            },
        });
    });
}

function broadcastPlayerPosition(session, data) {
    const clients = [...session.clients];
    clients.forEach(client => {
        client.send({
            type: 'client-position',
            clientId: data.clientId,
            x: data.x
        });
    });
}

function broadcastBallVelocity(session, data) {
    const clients = [...session.clients];
    clients.forEach(client => {
        client.send({
            type: 'ball-velocity',
            clientId: data.clientId,
            velocity: data.velocity
        });
    });
}

function broadcastGoalScored(session, data) {
    const clients = [...session.clients];
    clients.forEach(client => {
        client.send({
            type: 'goal-scored',
            clientId: data.clientId,
        });
    });
}

function broadcastGameStarted(session, _) {
    const clients = [...session.clients];
    clients.forEach(client => {
        client.send({
            type: 'game-started',
        });
    });
}

wss.on('connection', conn => {
    console.log("Connection established");
    const client = createClient(conn);
    console.log("Client id", client.id);

    conn.on('message', msg => {
        // console.log("Message received", msg);
        const data = JSON.parse(msg);

        // console.log("Sessions: ", sessions);

        if (data.type === 'create-session') {
            const session = createSession();
            session.join(client);
            client.send({
                type: 'session-created',
                id: session.id,
                you: client.id,
                playerOne: session.playerOne,
                playerTwo: session.playerTwo
            });
        } else if (data.type === 'join-session') {
            const session = getSession(data.id) || createSession(data.id);
            session.join(client);
            broadcastSession(session);
        } else if (data.type === 'player-position') {
            console.log('player-position', data);
            const session = getSession(data.id);
            broadcastPlayerPosition(session, data);
        } else if (data.type === 'goal-scored') {
            const session = getSession(data.id);
            const scorer = data.clientId;
            console.log(`${session}: goal scored by ${scorer}`);
            broadcastGoalScored(session, data);
        } else if (data.type === 'game-started') {
            const session = getSession(data.id);
            broadcastGameStarted(session, data);
        } else if (data.type === 'ball-velocity') {
            console.log('ball-velocity', data);
            const session = getSession(data.id);
            broadcastBallVelocity(session, data);
        }
    });

    conn.on('close', () => {
        console.log("Connection closed");
        const session = client.session;
        if (session) {
            session.leave(client);
            if (session.clients.size === 0) {
                sessions.delete(session.id);
            }
        }
        broadcastSession(session);
        console.log(sessions);
    });
});